<?php
//创建server对象，监听 服务器断开
//0.0.0.0 表示监听所有 IP 地址，一台服务器可能同时有多个 IP，如 127.0.0.1 本地回环 IP、192.168.1.100 局域网 IP、210.127.20.2 外网 IP，这里也可以单独指定监听一个 IP
$serv = new Swoole\Server('127.0.0.1',9501);

//监听连接事件

$serv->on('Connect',function ($serv,$fd){
   echo "client: Lconnect.\n";
   echo $serv;
   echo $fd;
});

//监听数据接收时间
$serv->on('Receive',function ($serv,$fd,$form_id,$data){
    $serv->send($fd,"Server:".$data);
});

//监听连接关闭事件
$serv->on('Close',function ($serv,$fd){
    echo "client:close";
});

//启动服务器
$serv->start();
